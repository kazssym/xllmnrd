Internals of Xenium LLMNR Responder
===================================

Interface managers
------------------

.. cpp:namespace:: xllmnrd

.. cpp:class:: interface_manager

.. cpp:class:: rtnetlink_interface_manager: public interface_manager
